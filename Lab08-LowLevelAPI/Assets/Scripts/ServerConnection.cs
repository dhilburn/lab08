﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.Networking;

public class ServerConnection : MonoBehaviour
{
    int serverSocketID = -1;
    int maxConnections = 10;
    byte unreliableChannelID;
    byte reliableChannelID;
    bool serverInitialized = false;

	// Use this for initialization
	void Start ()
    {
        GlobalConfig config_global = new GlobalConfig();
        config_global.ReactorModel = ReactorModel.FixRateReactor;
        config_global.ThreadAwakeTimeout = 10;

        ConnectionConfig config_channel = new ConnectionConfig();
        reliableChannelID = config_channel.AddChannel(QosType.ReliableSequenced);
        unreliableChannelID = config_channel.AddChannel(QosType.UnreliableSequenced);

        HostTopology hostTopology = new HostTopology(config_channel, maxConnections);

        NetworkTransport.Init(config_global);

        serverSocketID = NetworkTransport.AddHost(hostTopology, 7777);
        if (serverSocketID < 0)
            Debug.Log("Server socket creation failed");
        else
        {
            Debug.Log("Server socket created");
            serverInitialized = true;
        }
        
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!serverInitialized)
            return;

        int recHostID;                      // Who receives message
        int connectionID;                   // Who sent message
        int channelID;                      // What channel message was sent from
        int dataSize;                       // how large the message can be
        byte[] buffer = new byte[1024];     // the message
        byte error;                         // error flag

        NetworkEventType networkEvent = NetworkEventType.DataEvent;

        do
        {
            networkEvent = NetworkTransport.Receive(out recHostID, out connectionID, out channelID, buffer, 1024, out dataSize, out error);
            switch(networkEvent)
            {
                case (NetworkEventType.ConnectEvent):
                    if(recHostID.Equals(serverSocketID))
                    {
                        Debug.Log("Server: Player " + connectionID.ToString() + "connected");
                    }
                    break;
                case (NetworkEventType.DataEvent):
                    if(recHostID.Equals(serverSocketID))
                    {
                        Stream memoryStream = new MemoryStream(buffer);
                        BinaryFormatter formatter = new BinaryFormatter();
                        string message = formatter.Deserialize(memoryStream).ToString();
                        Debug.Log("Server: received data from player " + connectionID.ToString() + "! Message: " + message);
                        RespondMessage(message, connectionID);
                    }
                    break;
                case (NetworkEventType.DisconnectEvent):
                    if(recHostID.Equals(serverSocketID))
                    {
                        Debug.Log("Server: received disconnect from player " + connectionID.ToString());
                    }
                    break;
                default:
                    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
	}

    void SendMessage(string message, int target)
    {
        byte error;
        byte[] buffer = new byte[1024];
        Stream memoryStream = new MemoryStream(buffer);
        BinaryFormatter formatter = new BinaryFormatter();

        formatter.Serialize(memoryStream, message);
        NetworkTransport.Send(serverSocketID, target, reliableChannelID, buffer, (int)memoryStream.Position, out error);
        if(error != (byte)NetworkError.Ok)
        {
            NetworkError networkError = (NetworkError)error;
            Debug.Log("Error: " + networkError.ToString());
        }
    }

    void RespondMessage(string message, int playerID)
    {
        if(message.Equals("FirstConnect"))
        {
            Debug.Log("Player " + playerID.ToString() + " established their first connection");
            SendMessage("goto_NewScene", playerID);
            if(!SceneManager.GetActiveScene().name.Equals("Scene2"))
            {
                SceneManager.LoadScene("Scene2");
            }
        }
    }
}
